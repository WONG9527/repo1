package com.wangyw.cloud.service.impl;

import com.wangyw.cloud.entity.TradeInfo;
import com.wangyw.cloud.service.TradeInfoService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import static java.util.concurrent.Executors.newFixedThreadPool;

public class TradeInfoServiceImpl implements TradeInfoService {

    private static final  int CPU_CORE = Runtime.getRuntime().availableProcessors();

    int pageSize = 100;
    int currentPageLength = 0;
    int pageIndex = 0;


    private ExecutorService exe = newFixedThreadPool(CPU_CORE);
    @Override
    public List<TradeInfo> findAll() {
        Map<String,Object> queryParams  = new HashMap<>();
        do{
            int offset = pageIndex * pageSize;
            List<TradeInfo> tradeInfos = findTradeInfoBysPage(queryParams,offset,pageSize);
            if (null != tradeInfos && tradeInfos.size()>0){
                currentPageLength = tradeInfos.size();
                TradeInfoProcesserTask tradeInfoProcesserTask = new TradeInfoProcesserTask(tradeInfos);
                exe.execute(tradeInfoProcesserTask);
                pageIndex++;
            }else{
                System.out.println("Page Query TradeInfo Got NOTHING! Break query loop!");
                break;
            }
        }while (currentPageLength == pageSize);
        exe.shutdown();

        while(true) {
            if(exe.isTerminated()){
                System.out.println("分页式多线程处理数据完毕！");
                break;
            }
        }

        return null;
    }


    public List<TradeInfo> findTradeInfoBysPage(Map<String,Object> queryParams, int offset, int pageSize){
        List<TradeInfo> list = new ArrayList<>();

        return  list;



    }


}

class TradeInfoProcesserTask implements Runnable{

    private volatile List<TradeInfo> tradeInfos;

    public TradeInfoProcesserTask (List<TradeInfo> _tradeInfos){
        tradeInfos = _tradeInfos;
    }

    @Override
    public void run() {
        processTradeInfos();
    }

    private void processTradeInfos(){


    }
}


