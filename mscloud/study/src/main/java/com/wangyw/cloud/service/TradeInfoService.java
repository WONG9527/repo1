package com.wangyw.cloud.service;

import com.wangyw.cloud.entity.TradeInfo;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TradeInfoService {


    List<TradeInfo> findAll();
}
