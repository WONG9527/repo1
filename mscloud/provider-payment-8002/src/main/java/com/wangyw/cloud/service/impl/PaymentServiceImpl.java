package com.wangyw.cloud.service.impl;

import com.wangyw.cloud.dao.PaymentDao;
import com.wangyw.cloud.entity.Payment;
import com.wangyw.cloud.service.PaymentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 *  一般实现类都需要记得加上 @Service
 */
@Service
public class PaymentServiceImpl implements PaymentService {

    /**
     * @Resource 是 javax 包 等于 @Autowire 这个 spring 注解
     */
    @Resource
    PaymentDao paymentDao;

    @Override
    public int create(Payment payment) {
        return paymentDao.create(payment);
    }

    @Override
    public Payment getPaymentById(Long id) {
        return paymentDao.getPaymentById(id);
    }
}
